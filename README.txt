JSON:API Basic Site Settings

This simple module created to provide key site settings to
the frontend via JSON:API. It also has an API hook to add
some additional information to data. After enabling module,
you can open /jsonapi/site/site URL and read data. You must
use key_auth authentication provided by key_auth module, to
prevent unauthorized access.
