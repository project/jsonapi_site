<?php

/**
 * @file
 * Hooks and documentation related to json_api module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the information provided by jsonapi_site module.
 */
function hook_jsonapi_site_data_alter(&$data) {
  $data['additional'] = t('Some text');
}

/**
 * @} End of "addtogroup hooks".
 */
