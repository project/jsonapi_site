<?php

namespace Drupal\jsonapi_site\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Main controller.
 */
class JsonapiSiteController {

  /**
   * Return JSON data.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response.
   */
  public function returnJson(): JsonResponse {
    $config = \Drupal::config('system.site');
    $theme = \Drupal::config('system.theme');
    $theme_global = \Drupal::config('system.theme.global');
    $data = [
      "jsonapi" => [
        "version" => "1.0",
        "meta" => [
          "links" => [
            "self" => [
              "href" => "http://jsonapi.org/format/1.0/",
            ],
          ],
        ],
      ],
      "data" => [
        "type" => "site--site",
        "id" => $config->get('uuid'),
        "links" => [
          "self" => [
            "href" => \Drupal::request()->getSchemeAndHttpHost() .
            \Drupal::service('path.current')->getPath() .
            '/' . $config->get('uuid'),
          ],
        ],
        "attributes" => [
          "name" => $config->get('name'),
          "mail" => $config->get('mail'),
          "slogan" => $config->get('slogan'),
          "page_front" => $config->get('page.front'),
          "page_403" => $config->get('page.403'),
          "page_404" => $config->get('page.404'),
          "default_langcode" => $config->get('default_langcode'),
          "default_theme" => $theme->get('default'),
          "admin_theme" => $theme->get('admin'),
          "global_logo" => $theme_global->get('logo.path'),
          "global_favicon" => $theme_global->get('favicon.path'),
        ],
      ],
      "links" => [
        "self" => [
          "href" => \Drupal::request()->getUri(),
        ],
      ],
    ];

    \Drupal::moduleHandler()
      ->alter('jsonapi_site_data', $data['data']['attributes']);

    return new JsonResponse($data, '200',
      ['Content-Type' => 'application/vnd.api+json']);
  }

}
